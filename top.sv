
module top;
import shape_pkg::*;
  shape s;
  circle c;
  square sq;
  triangle t;
  
  initial begin
    s = new();
    sq = new();
    c = new();
    t = new();
    
    s = sq;
    
    s.draw();
    sq.draw();
    t.draw();
    c.draw();
  end
endmodule : top